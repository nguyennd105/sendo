package Login;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Login_blank_email {
	WebDriver driver;

	@Before
	public void setup() {
		System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
		driver = new FirefoxDriver();
		driver.get("https://www.sendo.vn/");

	}

	@Test
	public void Login_success() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 900);
		WebElement button = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"login\"]/span")));
		driver.findElement(By.xpath("//*[@id=\"login\"]/span")).click();
		WebDriverWait wait2 = new WebDriverWait(driver, 100);
		WebElement loginform = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='forgot_3Dko']")));
		driver.findElement(By.xpath("//div[@class='forgot_3Dko']")).click();
		WebDriverWait wait3 = new WebDriverWait(driver, 100);
		WebElement loginform2 = wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='labelGuideDisable_3Ga4']")));
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("Nguy3n123!");
		WebDriverWait wait4 = new WebDriverWait(driver, 100);
		driver.findElement(By.xpath("//button[@class='btnLogin_1Dve']")).click();
		WebDriverWait wait1 = new WebDriverWait(driver, 100);
		String expectedResult = "Vui lòng nhập địa chỉ email hoặc số điện thoại";
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[contains(text(),'Vui lòng nh')]")));
		String actualResult = driver.findElement(By.xpath("//p[contains(text(),'Vui lòng nh')]")).getText();
		Assert.assertEquals(expectedResult, actualResult);
	}

	@After
	public void tearDown() {
		driver.close();
	}
}
